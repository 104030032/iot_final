var map;

function init() {
    var cart_info = document.getElementById("cart_info");
    //get data from api
    var request = new XMLHttpRequest();
    var macaddr = sessionStorage.macaddr;
    cart_info.innerHTML = cart_info.innerHTML+" "+macaddr;
    request.open('GET', 'https://iot.martinintw.com/api/v1/data/' + macaddr, true);
    request.onload = function () {

        // Begin accessing JSON data here
        var data = JSON.parse(this.response);

        if (request.status >= 200 && request.status < 400) {
            var len = data.length - 1;

            for (i = 0; i < 3; i++) {
                //filter empty data
                while (data[len].lat == "" || data[len].lng == "") {
                    len--;
                }

                latlng = {
                    lat: parseFloat(data[len].lat),
                    lng: parseFloat(data[len].lng)
                };
                if (i == 0) {
                    map.panTo(latlng)
                    ICON = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
                } else {
                    ICON = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
                }

                console.log(parseFloat(data[len].lat) + "," + parseFloat(data[len].lng));
                var marker = new google.maps.Marker({
                    position: latlng,
                    icon: ICON,
                    map: map,
                });
                marker.setMap(map);
                len--;
            }
        } else {
            console.log('error');
        }
    }
    request.send();
}

window.onload = function () {
    init();
}

//map initialize
function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(24.7881, 120.9887),
        zoom: 18,
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}

