function init() {
    //get the dorm from last website

    var cart_id = document.getElementById("cart_id");
    var cart_time = document.getElementById("cart_time");

    //if available, get the number of cart and the due time and put it on HTML   

    if (sessionStorage.available == "true") {
        cart_id.innerHTML = "cart" + sessionStorage.cart_num;
        cart_time.innerHTML = sessionStorage.hr_due + ":" + sessionStorage.min_due;
        firebase.database().ref('cart_info/cart_' + sessionStorage.cart_num).update({
            due_time: sessionStorage.hr_due + ":" + sessionStorage.min_due,
            status: 1,
            user: sessionStorage.user
        });
    } else if (sessionStorage.reservable == "true") {
        cart_id.innerHTML = "cart" + sessionStorage.cart_num;
        cart_time.innerHTML = sessionStorage.hr_due + ":" + sessionStorage.min_due;
        firebase.database().ref('cart_info/cart_' + sessionStorage.cart_num).update({
            due_time: sessionStorage.hr_due + ":" + sessionStorage.min_due,
            status: 0,
            user: sessionStorage.user
        });
    } else {
        cart_id.innerHTML = "not available";
        cart_time.innerHTML = "not available";
    }


}


window.onload = function () {
    init();
}