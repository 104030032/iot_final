

function init() {

    var ID = document.getElementById('ID');
    var Phone = document.getElementById('phone');
    var LastDorm = document.getElementById('last_dorm');
    var LastRoom = document.getElementById('last_room');
    var NewDorm = document.getElementById('new_dorm');
    var NewRoom = document.getElementById('new_room');
    var TIME = document.getElementById('time');
    var submit = document.getElementById('submit-btn');

    submit.addEventListener('click', function () {
        var cart_num, available;
        //get value
        var id = ID.value;
        var phone = Phone.value;
        var lastdorm = LastDorm.options[LastDorm.selectedIndex].text;
        var newdorm = NewDorm.options[NewDorm.selectedIndex].text;
        var lastroom = LastRoom.value;
        var newroom = NewRoom.value;
        var time = parseInt(TIME.options[TIME.selectedIndex].text);
        //get submit time
        var date = new Date();
        var min_start = date.getMinutes();
        var hr_end = date.getHours();
        var min_end = min_start + time;

        if (min_end > 59) {
            min_end = min_end - 60;
            hr_end = hr_end + 1;
        }
        //get cart_id
        if (lastdorm == "義齋") {
            cart_num = "1";
        } else if (lastdorm == "禮齋") {
            cart_num = "2";
        } else if (lastdorm == "慧齋") {
            cart_num = "3";
        } else {
            cart_num = "0"
        }
        //check if the cart is available
        if (cart_num == "0") {
            available = false;
        } else {
            firebase.database().ref('cart_info/' + 'cart_' + cart_num).once('value').then(function (snapshot) {

                if (snapshot.val().status == -1)
                    available = true;
                else
                    available = false;
            });
        }
        //////////////////////////////////////////////////////////////////////////////////
        setTimeout(function () {
            if (id != "" && phone != "" && lastroom != "" && newroom != "" && LastDorm.selectedIndex != 0 && NewDorm.selectedIndex != 0) {
                console.log(available);
                if (available) {
                    var newcartref = firebase.database().ref('user_info/'+id);
                    newcartref.set({
                        ID: id,
                        number: phone,
                        原宿舍: lastdorm,
                        原房間: lastroom,
                        新宿舍: newdorm,
                        新房間: newroom,
                        hr_due: hr_end,
                        min_due: min_end
                    });
                    //
                    sessionStorage.hr_due = hr_end;
                    sessionStorage.min_due = min_end;
                    sessionStorage.cart_num = cart_num;
                    sessionStorage.user = id;
                    if (available) {
                        sessionStorage.available = "true";
                    } else {
                        sessionStorage.available = "false";
                    }

                } else {
                    sessionStorage.available = "false";
                }
                alert("成功送出");
                //
                ID.value = "";
                Phone.value = "";
                LastRoom.value = "";
                NewRoom.value = "";
                LastDorm.selectedIndex = 0;
                NewDorm.selectedIndex = 0;
                window.open("result.html");

            }
        }, 3000);
    });
}

window.onload = function () {
    init();
}