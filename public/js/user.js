function init() {

    var cart1 = document.getElementById('cart1');
    var cart2 = document.getElementById('cart2');
    var cart3 = document.getElementById('cart3');
    var user1 = document.getElementById('user1');
    var user2 = document.getElementById('user2');
    var user3 = document.getElementById('user3');
    var due_time1 = document.getElementById('due_time1');
    var due_time2 = document.getElementById('due_time2');
    var due_time3 = document.getElementById('due_time3');
    var return1 = document.getElementById('return1');
    var return2 = document.getElementById('return2');
    var return3 = document.getElementById('return3');
    var year = new Date().getFullYear();;
    var month = new Date().getMonth();
    var day = new Date().getDate();
    var timer1,timer2,timer3;

    //refresh
    firebase.database().ref('cart_info/' + 'cart_1').on('value', function(snapshot) {
        clearTimeout(timer1);
        check_status(snapshot.val().status,"1");
        user1.innerHTML = snapshot.val().user;
        due_time1.innerHTML = snapshot.val().due_time;
        var time1 = parsetime(due_time1.innerHTML);
        var countDownDate1 = new Date(year, month, day, time1.hour, time1.min, 00).getTime();        
        timer1 = count_down(countDownDate1, "1");
    });
    firebase.database().ref('cart_info/' + 'cart_2').on('value', function(snapshot) {
        clearTimeout(timer2);
        check_status(snapshot.val().status,"2");
        user2.innerHTML = snapshot.val().user;
        due_time2.innerHTML = snapshot.val().due_time;
        var time2 = parsetime(due_time2.innerHTML);
        var countDownDate2 = new Date(year, month, day, time2.hour, time2.min, 00).getTime();
        timer2 = count_down(countDownDate2, "2");
    });
    firebase.database().ref('cart_info/' + 'cart_3').on('value', function(snapshot) {
        clearTimeout(timer3);
        check_status(snapshot.val().status,"3");
        user3.innerHTML = snapshot.val().user;
        due_time3.innerHTML = snapshot.val().due_time;
        var time3 = parsetime(due_time3.innerHTML);
        var countDownDate3 = new Date(year, month, day, time3.hour, time3.min, 00).getTime();
        timer3 = count_down(countDownDate3, "3");
    });

    //connect cart to its location
    cart1.addEventListener('click', function () {
        sessionStorage.macaddr = "12345617";
        window.open("map.html");
    });

    cart2.addEventListener('click', function () {
        sessionStorage.macaddr = "12345618";
        window.open("map.html");
    });

    cart3.addEventListener('click', function () {
        sessionStorage.macaddr = "12345619";
        window.open("map.html");
    });
    //delete
    return1.addEventListener('click', function () {
        clearTimeout(timer1);
        firebase.database().ref('user_info/' + user1.innerHTML).remove();
        firebase.database().ref('cart_info/cart_1').update({
            due_time: "unknown",
            status: -1,
            user: "unknown"
        });
    });

    return2.addEventListener('click', function () {
        clearTimeout(timer2);
        firebase.database().ref('user_info/' + user2.innerHTML).remove();
        firebase.database().ref('cart_info/cart_2').update({
            due_time: "unknown",
            status: -1,
            user: "unknown"
        });
        
    });

    return3.addEventListener('click', function () {
        clearTimeout(timer3);
        firebase.database().ref('user_info/' + user3.innerHTML).remove();
        firebase.database().ref('cart_info/cart_3').update({
            due_time: "unknown",
            status: -1,
            user: "unknown"
        });
    });
}

window.onload = function () {
    init();
}

function count_down(countDownDate, id) {

    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("count_down" + id).innerHTML = hours + "h " + minutes + "m " + seconds + "s ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("count_down" + id).innerHTML = "EXPIRED";
        }
    }, 1000);
    return x;
}

function parsetime(str) {
    var pos = str.search(":");
    var hour = str.slice(0, pos);
    var min = str.slice(pos + 1, str.length);
    return {
        hour,
        min
    };
}

function check_status(status,id)
{
    var cart=document.getElementById("status" + id);
    if(status==-1)cart.classList.add("idle");
    else if(status==0) cart.classList.add("preserve");
    else cart.classList.add("in_use");
}